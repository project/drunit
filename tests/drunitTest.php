<?php

class DrunitTest extends Drunit_Framework_TestCase {
  protected function setUp() {}

  public function testDrunitClassExtends() {
    // this should evaluate to TRUE
    $this->assertTrue(drunit_class_extends('DrunitClassExtendsDummyClass2', 'DrunitClassExtendsDummyClass'));
    // the other way around shouldn return FALSE
    $this->assertFalse(drunit_class_extends('DrunitClassExtendsDummyClass', 'DrunitClassExtendsDummyClass2'));
    // FALSE
    $this->assertFalse(drunit_class_extends('DrunitClassExtendsDummyClass3', 'DrunitClassExtendsDummyClass'));
  }
}

class DrunitClassExtendsDummyClass extends DrunitClassExtendsDummyClass2 {}
class DrunitClassExtendsDummyClass2 {}
class DrunitClassExtendsDummyClass3 {}
