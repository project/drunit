<?php

$_SERVER['REDIRECT_STATUS'] = '200';
$_SERVER['HTTP_HOST'] = DRUNIT_HOST;
$_SERVER['HTTP_CONNECTION'] = 'keep-alive';
$_SERVER['HTTP_CACHE_CONTROL'] = 'no-cache';
$_SERVER['HTTP_PRAGMA'] = 'no-cache';
$_SERVER['HTTP_ACCEPT'] = 'application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
$_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.3 (KHTML, like Gecko) Chrome/6.0.472.51 Safari/534.3';
$_SERVER['HTTP_ACCEPT_ENCODING'] = 'gzip,deflate,sdch';
$_SERVER['HTTP_ACCEPT_LANGUAGE'] = 'en-US,en;q=0.8';
$_SERVER['HTTP_ACCEPT_CHARSET'] = 'ISO-8859-1,utf-8;q=0.7,*;q=0.3';
$_SERVER['HTTP_COOKIE'] = '';
$_SERVER['PATH'] = '/usr/local/bin:/usr/bin:/bin';
$_SERVER['SERVER_SIGNATURE'] = '<address>Apache/2.2.14 (Ubuntu) Server at ' 
  . DRUNIT_HOST . ' Port 80</address> 
';
$_SERVER['SERVER_SOFTWARE'] = 'Apache/2.2.14 (Ubuntu)';
$_SERVER['SERVER_NAME'] = DRUNIT_HOST;
$_SERVER['SERVER_ADDR'] = '127.0.1.1';
$_SERVER['SERVER_PORT'] = '80';
$_SERVER['REMOTE_ADDR'] = '127.0.1.1';
$_SERVER['DOCUMENT_ROOT'] = DRUNIT_FILEPATH_BASE;
$_SERVER['SERVER_ADMIN'] = '[no address given]';
$_SERVER['SCRIPT_FILENAME'] = DRUNIT_FILEPATH_BASE . '/index.php';
$_SERVER['REMOTE_PORT'] = '34237';
$_SERVER['REDIRECT_QUERY_STRING'] = 'q=' . DRUNIT_QUERY;
$_SERVER['REDIRECT_URL'] = '/user/front';
$_SERVER['GATEWAY_INTERFACE'] = 'CGI/1.1';
$_SERVER['SERVER_PROTOCOL'] = 'HTTP/1.1';
$_SERVER['REQUEST_METHOD'] = 'GET';
$_SERVER['QUERY_STRING'] = 'q=' . DRUNIT_QUERY;
$_SERVER['REQUEST_URI'] = '/' . DRUNIT_QUERY;
$_SERVER['SCRIPT_NAME'] = '/index.php';
$_SERVER['PHP_SELF'] = '/index.php';
$_SERVER['REQUEST_TIME'] = '1283767844';
