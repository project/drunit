#!/usr/bin/php
<?php
define('DRUNIT_MODULES_FILEPATH', realpath(dirname(__FILE__) . '/../..'));

function drunit_get_dirs() {
  $modules = glob(DRUNIT_MODULES_FILEPATH . '/*');
  $dirs = array();
  foreach ($modules as $module) {
    $drunit_dir = $module . '/drunit';
    if (is_dir($drunit_dir)) {
      $dirs[] = $drunit_dir;
    }
  }
  return $dirs;
}

function drunit_render_xml($dirs) {
  ob_start(); ?>
<phpunit
  backupStaticAttributes="false"
  colors="true"
  bootstrap="<?=DRUNIT_MODULES_FILEPATH?>/drunit/bootstrap.php"
>
  <testsuite name="Drunit testsuite">
<?foreach($dirs as $dir):?>
  <directory><?=$dir?></directory>
<?endforeach;?>
  </testsuite>
</phpunit>
  <? return ob_get_clean();
}

file_put_contents(DRUNIT_MODULES_FILEPATH . '/drunit/config/config.xml', 
  drunit_render_xml(drunit_get_dirs()));
