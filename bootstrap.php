<?php

// Globals.
define('DRUNIT_FILEPATH_BASE', realpath(dirname(__FILE__) . '/../../../..'));
define('DRUNIT_FILEPATH_DRUNIT', 
  DRUNIT_FILEPATH_BASE . '/sites/all/modules/drunit');
define('DRUNIT_FILEPATH_FILES', 
  DRUNIT_FILEPATH_BASE . '/sites/default/files');
define('DRUNIT_FILEPATH_FILES_DRUNIT', 
  DRUNIT_FILEPATH_FILES . '/drunit');
define('DRUNIT_FILEPATH_FILES_SCREENSHOTS', 
  DRUNIT_FILEPATH_FILES_DRUNIT . '/screenshots');

// Create directories if neccessary.
if (!is_dir(DRUNIT_FILEPATH_FILES_DRUNIT)) {
  if (!mkdir(DRUNIT_FILEPATH_FILES_DRUNIT)) {
    die('The directory ' . DRUNIT_FILEPATH_FILES 
      . " needs to be writeable.\n");
  }
}
if (!is_dir(DRUNIT_FILEPATH_FILES_SCREENSHOTS)) {
  if (!mkdir(DRUNIT_FILEPATH_FILES_SCREENSHOTS)) {
    die('The directory ' . DRUNIT_FILEPATH_FILES_DRUNIT 
      . " needs to be writeable.\n");
  }
}

// Includes.
require_onceDRUNIT_FILEPATH_DRUNIT . '/settings.php';
require_onceDRUNIT_FILEPATH_DRUNIT . '/_SERVER.php';
require_onceDRUNIT_FILEPATH_BASE . '/constants.php';
require_onceDRUNIT_FILEPATH_BASE . '/includes/bootstrap.inc';

// Bootstrap drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
db_query("TRUNCATE {cache}");

// More globals.
define('DRUNIT_URL_BASE', 'http://' . DRUNIT_HOST);
define('DRUNIT_URL_FILES_SCREENSHOTS', 
  'http://' . DRUNIT_HOST . '/sites/default/files/drunit/screenshots');

// More includes.
require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Extensions/SeleniumTestCase.php';
require_onceDRUNIT_FILEPATH_DRUNIT . '/Extensions/SeleniumTestCase.php';
require_onceDRUNIT_FILEPATH_DRUNIT . '/Framework/TestCase.php';

