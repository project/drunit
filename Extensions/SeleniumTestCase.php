<?php

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

class Drunit_Extensions_SeleniumTestCase extends PHPUnit_Extensions_SeleniumTestCase {

  protected $drupal_user;
  protected $drupal_password;

  /**
   * The current user logged in using the internal browser.
   *
   * @var bool
   */
  protected $loggedInUser = FALSE;

  protected $captureScreenshotOnFailure = TRUE;
  protected $screenshotPath;
  protected $screenshotUrl;
  
  protected function setUp() {
    $drunit_conf = variable_get('drunit_conf', array());
    if (empty($drunit_conf)) {
      $message = <<<MSG
      \n
      Please add the following in your settings.php file
      \$conf['drunit_selenium_conf'] = array(
        // Drupal user settings to be used by selenium
        'drupal_user' => 'admin',
        'drupal_password' => 'pass',
        // Selenium settings
        'sel_browser' => '*firefox',
        'sel_browser_url' => 'http://example.com/', // url where your Drupal installation resides
        'sel_host' => 'localhost', // hostname where the selenium server is running
        'sel_port' => 4444,
        'sel_timeout' => 30000,
        'sel_speed' => 0,
      );\n\n
MSG;
      exit($message);
    }

    $this->drupal_user = $drunit_conf['drupal_user'];
    $this->drupal_password = $drunit_conf['drupal_password'];

    $this->setBrowser($drunit_conf['sel_browser']);
    $this->setBrowserUrl($drunit_conf['sel_browser_url']);
    $this->setHost($drunit_conf['sel_host']);
    $this->setPort($drunit_conf['sel_port']);
    $this->setTimeout($drunit_conf['sel_timeout']);
    $this->setSpeed($drunit_conf['sel_speed']);
  }

  /**
   * Adds support for *AndAjaxWait function calls.
   * We can call methods like clickAndAjaxWait()
   * @link http://agilesoftwaretesting.com/?p=111
   */
  public function __call($command, $arguments) {
    if (preg_match('/AndAjaxWait$/', $command)) {
      $command = str_replace('AndAjaxWait', '', $command);
      if (is_callable(array($this, $command))) {
        $return = call_user_func_array(array($this, $command), $arguments);
        $this->waitForAjaxFinish();
        return $return;
      }
    }
    return parent::__call($command, $arguments);
  }

  static public function LogToC($string = "") {
    print "$string;\n";
  }

  /**
   * Waits until there is no more active AJAX calls or until it reaches $timeout
   */
  function waitForAjaxFinish($timeout = 5000) {
    $this->waitForCondition('selenium.browserbot.getCurrentWindow().jQuery.active == 0', $timeout);
  }

  function seleniumLogout() {
    try {
      $this->openAndWait('/logout');
    } catch (Exception $e) {

    }
  }

  function seleniumLogin() {
    static $tries = 0;
    $tries++;
    try {
      $this->openAndWait("/user");
      if (!$this->isElementPresent('id=edit-name')) {
        if ($tries > 2) {
          exit('Could not login, #edit-name form element not found.');
        }
        $this->seleniumLogout();
        $this->seleniumLogin();
        return;
      }
      $this->type("edit-name", $this->drupal_user);
      $this->type("edit-pass", $this->drupal_password);
      $this->click("edit-submit");
      $this->waitForPageToLoad();
    } catch (Exception $e) {
      if ($tries > 2) {
        exit('Could not login.');
      }
    }
  }

  function seleniumCreateNode($settings = array()) {
    $settings += array(
      'type' => 'page',
      'edit-title' => $this->randomName(),
    );
    $node_add_page = "/node/add/" . str_replace("_", "-", $settings['type']);
    unset($settings['type']);
    $this->openAndWait($node_add_page);
    foreach ($settings as $key => $value) {
      $this->type($key, $value);
    }
    $this->clickAndWait("edit-submit");
    $this->clickAndWait("link=Edit");
    preg_match('/\/node\/(\d+)\/edit$/', $this->getLocation(), $matches);
    return node_load($matches[1], NULL, TRUE);
  }

  /**
   * Helper function. Recursively checks that
   * all the keys of a table are present in a second and have the same value.
   */
  function _compareArrayForChanges($fields, $data, $message, $prefix = '') {
    foreach ($fields as $key => $value) {
      $newprefix = ($prefix == '') ? $key : $prefix . '][' . $key;
      if (is_array($value)) {
        $compare_to = isset($data[$key]) ? $data[$key] : array();
        $this->_compareArrayForChanges($value, $compare_to, $message, $newprefix);
      } else {
        $this->assertEquals($value, $data[$key], t($message, array('!key' => $newprefix)));
      }
    }
  }

  /**
   * Creates a node based on default settings.
   *
   * @param $settings
   *   An associative array of settings to change from the defaults, keys are
   *   node properties, for example 'title' => 'Hello, world!'.
   * @return
   *   Created node object.
   */
  protected function drupalCreateNode($settings = array()) {
    // Populate defaults array.
    $settings += array(
      'body' => $this->randomName(32),
      'title' => $this->randomName(8),
      'comment' => 2,
      'changed' => time(),
      'format' => FILTER_FORMAT_DEFAULT,
      'moderate' => 0,
      'promote' => 0,
      'revision' => 1,
      'log' => '',
      'status' => 1,
      'sticky' => 0,
      'type' => 'page',
      'revisions' => NULL,
      'taxonomy' => NULL,
    );

    // Use the original node's created time for existing nodes.
    if (isset($settings['created']) && !isset($settings['date'])) {
      $settings['date'] = format_date($settings['created'], 'custom', 'Y-m-d H:i:s O');
    }

    // If the node's user uid is not specified manually, use the currently
    // logged in user if available, or else the user running the test.
    if (!isset($settings['uid'])) {
      if ($this->loggedInUser) {
        $settings['uid'] = $this->loggedInUser->uid;
      } else {
        global $user;
        $settings['uid'] = $user->uid;
      }
    }

    $node = (object) $settings;
    node_save($node);

    db_query('UPDATE {node_revisions} SET uid = %d WHERE vid = %d', $node->uid, $node->vid);
    return $node;
  }

  function drupalLogin($uid = 1) {
    global $user;
    $user = user_load(array('uid' => $uid));
    $this->loggedInUser = $user;
  }

  function drupalFlushAllCaches() {
    // Flush all caches; no need to re-implement this.
    module_load_include('inc', 'system', 'system.admin');
    $form = $form_state = array();
    system_clear_cache_submit($form, $form_state);
  }

  /**
   * Generates a random string of ASCII characters of codes 32 to 126.
   *
   * The generated string includes alpha-numeric characters and common misc
   * characters. Use this method when testing general input where the content
   * is not restricted.
   *
   * @param $length
   *   Length of random string to generate which will be appended to $db_prefix.
   * @return
   *   Randomly generated string.
   */
  public static function randomString($length = 8) {
    global $db_prefix;

    $str = '';
    for ($i = 0; $i < $length; $i++) {
      $str .= chr(mt_rand(32, 126));
    }
    return str_replace('simpletest', 's', $db_prefix) . $str;
  }

  /**
   * Generates a random string containing letters and numbers.
   *
   * The letters may be upper or lower case. This method is better for
   * restricted inputs that do not accept certain characters. For example,
   * when testing input fields that require machine readable values (ie without
   * spaces and non-standard characters) this method is best.
   *
   * @param $length
   *   Length of random string to generate which will be appended to $db_prefix.
   * @return
   *   Randomly generated string.
   */
  public static function randomName($length = 8) {
    global $db_prefix;

    $values = array_merge(range(65, 90), range(97, 122), range(48, 57));
    $max = count($values) - 1;
    $str = '';
    for ($i = 0; $i < $length; $i++) {
      $str .= chr($values[mt_rand(0, $max)]);
    }
    return str_replace('simpletest', 's', $db_prefix) . $str;
  }

}