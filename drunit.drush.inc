<?php
/**
 * @file
 *   Drunit module drush integration.
 */

/**
 * Implementation of hook_drush_command().
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function drunit_drush_command() {
  $items = array();

  $items['drunit-list'] = array(
    'description' => "List all the available phpunits for your site.",
  );

  $items['drunit-run'] = array(
    'arguments' => array(
      'class' => 'PHPUnitTest to run (the class name).',
    ),
  );

  $items['drunit-run-all'] = array(
    'description' => "Run all PHPUnit tests.",
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function drunit_drush_help($section) {
  switch ($section) {
    case 'drush:drunit-list':
      return dt("List all the available phpunits for your site.");
    case 'drush:drunit-run-all':
      return dt("Run all PHPUnit tests.");
  }
}

function drush_drunit_list() {
  $classes = drunit_test_get_all_classes();
  $rows = array(array(dt('Module Name'), dt('Class')));
  foreach ($classes as $class) {
    $rows[] = array(
      $class['module_name'],
      $class['class'],
    );
  }
  drush_print_table($rows, TRUE);
}

function drush_drunit_run_all() {
  drush_drunit_include_files();

  $classes = drunit_test_get_all_classes();

  foreach ($classes as $class) {
    _drush_drunit_run_test_suite($class);
  }
}

function drush_drunit_run() {
  $args = func_get_args();

  if (empty($args)) {
    exit("Please specify a UnitTest.");
  }

  drush_drunit_include_files();

  $drupal_root = drush_locate_root();
  $classes = drunit_test_get_all_classes();

  $listener = new DrunitTestListener();

  foreach ($classes as $class) {
    if (!in_array($class['class'], $args)) {
      continue;
    }
    _drush_drunit_run_test_suite($class);
  }
}

function _drush_drunit_run_test_suite($class) {
  $drupal_root = drush_locate_root();

  $listener = new DrunitTestListener();

  $suite = new PHPUnit_Framework_TestSuite($class['class']);
  $suite->addTestFile($drupal_root . '/' . $class['file']);
  return PHPUnit_TextUI_TestRunner::run($suite, array(
      'backupGlobals' => FALSE,
      'backupStaticAttributes' => FALSE,
      'listeners' => array($listener),
      'colors' => TRUE,
      'processIsolation' => TRUE,
    )
  );
}

function drunit_test_get_all_classes() {
  drush_drunit_include_files();

  $classes = array();
  $files = module_rebuild_cache();
  foreach ($files as $file) {
    $directory = dirname($file->filename);
    $test_files = file_scan_directory($directory, 'Test\.php$', array('.', '..', 'CVS'), FALSE, FALSE);
    $test_files += file_scan_directory($directory . '/tests', 'Test\.php$');

    foreach ($test_files as $test_file) {
      $pre = get_declared_classes();
      require_once $test_file->filename;
      $post = get_declared_classes();

      $classes_new = array_values(array_diff($post, $pre));
      foreach ($classes_new as $class) {
        if (drunit_class_extends('PHPUnit_Framework_TestCase', $class) && method_exists($class, 'setUp')) {
          $classes[$class] = array(
            'file' => $test_file->filename,
            'class' => $class,
            'module' => $file->name,
            'module_name' => $file->info['name'],
          );
        }
      }
    }
  }

  return $classes;
}

function drush_drunit_include_files() {
  // include PHPUnit files
  require_once 'PHPUnit/Autoload.php';
  require_once 'PHPUnit/TextUI/TestRunner.php';
  require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

  // include Drunit files
  $base_dir = realpath(dirname(__FILE__));
  require_once $base_dir . '/DrunitTestListener.inc';
  require_once $base_dir . '/Framework/TestCase.php';
  require_once $base_dir . '/Extensions/SeleniumTestCase.php';
}

/**
 * Check if a class extends or implements a specific class/interface
 *
 * @param $search
 *   The class or interface name to look for
 * @param $className
 *   The class name of the object to compare to
 * @return bool
 */
function drunit_class_extends($search, $className) {
  $class = new ReflectionClass($className);
  if (FALSE === $class) {
    return FALSE;
  }
  do {
    $name = $class->getName();
    if ($search == $name) {
      return TRUE;
    }
    $class = $class->getParentClass();
  } while (FALSE !== $class);
  return FALSE;
}

